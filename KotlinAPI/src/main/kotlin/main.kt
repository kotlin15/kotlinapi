import com.dao.UserDao
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.Javalin

fun main(args: Array<String>) {

    val userDao = UserDao()

    val app = Javalin.create().apply {
        exception(Exception::class.java) { e, ctx -> e.printStackTrace() }
        error(404) { ctx -> ctx.json("not found") }
    }.start(7000)
    app.routes {

        app.get("/") { ctx ->
            ctx.result("Hello World")
        }

        get("/users") { ctx ->
            ctx.json(userDao.users)
        }

        get("/users/:id") { ctx ->
            ctx.json(userDao.findById(ctx.pathParam("id").toInt())!!)
        }

        get("/users/email/:email") { ctx ->
            ctx.json(userDao.findByEmail(ctx.pathParam("email"))!!)
        }

        post("/users") { ctx ->
            val user = ctx.body<UserDao.Users>()
            userDao.save(name = user.name, email = user.email)
            ctx.status(201)
        }

        patch("/users/:id") { ctx ->
            val user = ctx.body<UserDao.Users>()
            userDao.update(
                id = ctx.pathParam("id").toInt(),
                user = user
            )
            ctx.status(204)
        }

        delete("/users/:id") { ctx ->
            userDao.delete(ctx.pathParam("id").toInt())
            ctx.status(204)
        }
    }
}