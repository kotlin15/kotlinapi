
package com.dao
import com.util.StringUtil
import java.io.IOException

class UserDao {

    data class Users(val id: Int, val name: String, val email: String)
    val users:HashMap<Int,Users> = HashMap<Int,Users>()

    @Throws(IOException::class)
    fun save(name: String, email: String){
        val id = StringUtil.nex()
        users.put(id, Users(id = id, name = name, email = email))
    }

    @Throws(IOException::class)
    fun findById(id: Int): Users? {
        return users[id]
    }

    @Throws(IOException::class)
    fun findByEmail(email: String): Users? {
        return users.values.find { it.email == email }
    }

    @Throws(IOException::class)
    fun update(id: Int, user: Users) {
        users.put(id, Users(id = id, name = user.name, email = user.email))
    }

    @Throws(IOException::class)
    fun delete(id: Int) {
        users.remove(id)
    }
}